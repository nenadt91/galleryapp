package com.typeqast.galleryapp.homescreen

import androidx.lifecycle.*
import com.typeqast.domain.models.AlbumDOM
import com.typeqast.domain.models.GalleryEntityDOM
import com.typeqast.domain.models.ShowAlbum
import com.typeqast.domain.wrappers.Resource
import com.typeqast.galleryapp.GalleryApplication
import com.typeqast.galleryapp.R
import com.typeqast.galleryapp.injection.Injector.provideGalleryUseCase


class HomeScreenViewModel : ViewModel() {
    private val fetchGalleryEvent = MutableLiveData<Boolean>()
    private val storedGalleryAlbum = MutableLiveData(
        ShowAlbum(
            0,
            GalleryApplication.app.resources.getString(R.string.pick_image),
            ""
        )
    )
    private val fetchGalleryAlbums: LiveData<Resource<List<GalleryEntityDOM>>> =
        Transformations.switchMap(fetchGalleryEvent) {
            liveData {
                emitSource(
                    provideGalleryUseCase().execute(
                        GalleryApplication.app.checkInternetConnection()
                    )
                )
            }
        }

    private val signalViewAction = MutableLiveData<Pair<ShowAlbum, List<AlbumDOM>>>()

    init {
        dispatchEvent(Events.FETCH_ALBUMS, null)
    }

    fun dispatchEvent(event: Events, data: Any?) {
        when (event) {
            Events.FETCH_ALBUMS -> fetchGalleryEvent.value = true
            Events.SET_ALBUM -> storedGalleryAlbum.value = data as ShowAlbum
            Events.OPEN_PREVIEW_ALBUM_SCREEN -> openNewScreenViewAction(
                storedGalleryAlbum.value,
                fetchGalleryAlbums.value
            )
        }
    }

    fun fetchGalleryAlbums() = fetchGalleryAlbums
    fun setAlbumSelection() = storedGalleryAlbum
    fun navigateToSelectedAlbum() = signalViewAction

    private fun openNewScreenViewAction(
        selectedAlbum: ShowAlbum?,
        value: Resource<List<GalleryEntityDOM>>?
    ) {
        val item: GalleryEntityDOM? = value?.data?.find { it.id == selectedAlbum?.id }
        if (item != null && selectedAlbum != null) {
            signalViewAction.value = Pair(selectedAlbum, item.album)
        }
    }

    enum class Events {
        FETCH_ALBUMS,
        SET_ALBUM,
        OPEN_PREVIEW_ALBUM_SCREEN
    }
}