package com.typeqast.galleryapp.previewalbum

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.typeqast.core.ui.ICoreView
import com.typeqast.domain.models.PhotoDOM
import com.typeqast.galleryapp.R
import com.typeqast.galleryapp.databinding.FragmentPreviewAlbumBinding
import com.typeqast.galleryapp.utils.ResponseWrapper
import com.typeqast.galleryapp.utils.ui.BaseFragment

class PreviewPhotoFragment : BaseFragment<FragmentPreviewAlbumBinding, PreviewAlbumViewModel>() {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadPhoto()
    }

    override fun provideTAG(): String {
        return PreviewPhotoFragment::class.java.simpleName
    }

    override fun provideViewModel(): PreviewAlbumViewModel {
        return ViewModelProvider(requireActivity())[PreviewAlbumViewModel::class.java]
    }

    override fun provideLayout(): Int {
        return R.layout.fragment_preview_photo
    }

    private fun loadPhoto() {
        getViewModel()
            .dispatchEvent(Pair(PreviewAlbumViewModel.Events.FETCH_PHOTO, null))
        getViewModel()
            .fetchAlbumPhoto()
            .observe(viewLifecycleOwner,
                object : ResponseWrapper<PhotoDOM?, ICoreView>(this) {
                    override fun onSuccess(data: PhotoDOM?) {
                    }
                })
    }
}