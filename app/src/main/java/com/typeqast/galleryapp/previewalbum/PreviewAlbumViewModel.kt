package com.typeqast.galleryapp.previewalbum

import androidx.lifecycle.*
import com.typeqast.domain.models.GalleryEntityDOM
import com.typeqast.domain.models.PhotoDOM
import com.typeqast.domain.models.ShowPhoto
import com.typeqast.domain.wrappers.Resource
import com.typeqast.galleryapp.GalleryApplication
import com.typeqast.galleryapp.R
import com.typeqast.galleryapp.injection.Injector

class PreviewAlbumViewModel : ViewModel() {

    private var albumId: Int = 0
    private val signalViewAction = MutableLiveData<Pair<Actions, Any?>>()
    private val fetchAlbumEvent = MutableLiveData<Int>()
    private val fetchPhotoEvent = MutableLiveData<String>()
    private val gallerySelectedPhotoFromAlbum = MutableLiveData(
        ShowPhoto(
            "",
            GalleryApplication.app.resources.getString(R.string.pick_image),
            ""
        )
    )
    private val fetchAlbumAction: LiveData<Resource<GalleryEntityDOM?>> =
        Transformations.switchMap(fetchAlbumEvent) {
            liveData {
                emitSource(
                    Injector.provideAlbumUseCase().execute(
                        GalleryApplication.app.checkInternetConnection(),
                        it
                    )
                )
            }
        }

    private val fetchPhotoAction: LiveData<Resource<PhotoDOM>> =
        Transformations.switchMap(fetchPhotoEvent) {
            liveData {
                emitSource(
                    Injector.providePhotoUseCase().execute(
                        GalleryApplication.app.checkInternetConnection(),
                        it
                    )
                )
            }
        }


    fun dispatchEvent(pair: Pair<Events, Any?>) {
        when (pair.first) {
            Events.STORE_ALBUM_ID -> storeAlbumId(pair.second as Int)
            Events.FETCH_SELECTED_ALBUM -> loadAlbumPhotos()
            Events.STORE_ALBUM_SELECTION -> previewSelectedAlbumPhoto(pair.second)
            Events.NAVIGATE_TO_PREVIEW_FRAGMENT -> sendViewActon(
                Pair(
                    Actions.NAVIGATE_TO_SHOW_ALBUM,
                    albumId
                )
            )
            Events.SHOW_SELECTED_IMAGE -> navigateToPreviewImageScreen()
            Events.FETCH_PHOTO -> loadPhoto()
        }
    }


    fun fetchAlbums() = fetchAlbumAction
    fun fetchAlbumPhoto() = fetchPhotoAction
    fun observeViewActions() = signalViewAction
    fun storeAlbumSelection() = gallerySelectedPhotoFromAlbum

    private fun loadAlbumPhotos() {
        fetchAlbumEvent.value = albumId
    }

    private fun loadPhoto() {
        fetchPhotoEvent.value = gallerySelectedPhotoFromAlbum.value?.id ?: ""
    }

    private fun sendViewActon(action: Pair<Actions, Any?>) {
        signalViewAction.value = action
    }

    private fun navigateToPreviewImageScreen() {
        sendViewActon(Pair(Actions.NAVIGATE_TO_SHOW_PHOTO, gallerySelectedPhotoFromAlbum.value))
    }


    private fun previewSelectedAlbumPhoto(data: Any?) {
        gallerySelectedPhotoFromAlbum.value = data as ShowPhoto
    }

    private fun storeAlbumId(id: Int) {
        albumId = id
    }

    enum class Events {
        STORE_ALBUM_SELECTION,
        STORE_ALBUM_ID,
        FETCH_SELECTED_ALBUM,
        NAVIGATE_TO_PREVIEW_FRAGMENT,
        SHOW_SELECTED_IMAGE,
        FETCH_PHOTO
    }

    enum class Actions {
        NAVIGATE_TO_SHOW_ALBUM,
        NAVIGATE_TO_SHOW_PHOTO
    }
}