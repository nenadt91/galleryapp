package com.typeqast.galleryapp.previewalbum

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.typeqast.core.ui.ICoreView
import com.typeqast.domain.models.AlbumDOM
import com.typeqast.domain.models.GalleryEntityDOM
import com.typeqast.domain.models.ShowPhoto
import com.typeqast.domain.wrappers.NetworkPolicy
import com.typeqast.galleryapp.R
import com.typeqast.galleryapp.databinding.FragmentPreviewAlbumBinding
import com.typeqast.galleryapp.utils.ResponseWrapper
import com.typeqast.galleryapp.utils.ui.BaseFragment

class AlbumOverviewFragment : BaseFragment<FragmentPreviewAlbumBinding, PreviewAlbumViewModel>(),
    AlbumOverviewAdapter.OnClickListener, View.OnClickListener,
    SwipeRefreshLayout.OnRefreshListener {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUiComponents()
        loadPhotos()
    }

    override fun provideTAG(): String {
        return AlbumOverviewFragment::class.java.name
    }

    override fun provideViewModel(): PreviewAlbumViewModel {
        return ViewModelProvider(requireActivity())[PreviewAlbumViewModel::class.java]
    }

    override fun provideLayout(): Int {
        return R.layout.fragment_preview_album
    }

    private fun setupUiComponents() {
        getBinding()
            .rvGallery
            .apply {
                adapter = AlbumOverviewAdapter(this@AlbumOverviewFragment)
                layoutManager = LinearLayoutManager(
                    context, RecyclerView.HORIZONTAL,
                    false
                )
            }

        getBinding()
            .card
            .setOnClickListener(this@AlbumOverviewFragment)

        getBinding()
            .srLayout
            .setOnRefreshListener(this)
    }

    private fun loadPhotos() {
        getViewModel()
            .observeViewActions()
            .observe(this.viewLifecycleOwner,
                { dispatchViewActions(it.first, it.second) })

        getViewModel()
            .fetchAlbums()
            .observe(
                viewLifecycleOwner,
                object : ResponseWrapper<GalleryEntityDOM?, ICoreView>(this) {
                    override fun updateUI(status: Int) {
                        when (status) {
                            NetworkPolicy.Status.LOADING -> {
                                getBinding()
                                    .pbLoading.visibility = View.VISIBLE
                                getBinding()
                                    .card.isEnabled = false
                            }
                            else -> {
                                getBinding()
                                    .srLayout
                                    .isRefreshing = false
                                getBinding()
                                    .pbLoading
                                    .visibility = View.GONE
                            }
                        }
                    }

                    override fun onSuccess(data: GalleryEntityDOM?) {
                        ((getBinding()
                            .rvGallery.adapter) as AlbumOverviewAdapter)
                            .setItems(data?.album)
                        onItemClicked(data?.album?.get(0))
                        getBinding()
                            .card.isEnabled = true
                    }
                }
            )

        getViewModel()
            .dispatchEvent(Pair(PreviewAlbumViewModel.Events.FETCH_SELECTED_ALBUM, null))
    }

    private fun dispatchViewActions(actions: PreviewAlbumViewModel.Actions, data: Any?) {}

    override fun onItemClicked(item: AlbumDOM?) {
        if (item == null) {
            return
        }
        getViewModel().dispatchEvent(
            Pair(
                PreviewAlbumViewModel.Events.STORE_ALBUM_SELECTION,
                ShowPhoto(
                    item.imageUrlPath,
                    item.name,
                    item.thumbnailImage
                )
            )
        )
    }

    override fun onClick(view: View?) {
        getViewModel()
            .dispatchEvent(Pair(PreviewAlbumViewModel.Events.SHOW_SELECTED_IMAGE, null))
    }

    override fun onRefresh() {
        if (getBinding().srLayout.isRefreshing) {
            getViewModel()
                .dispatchEvent(Pair(PreviewAlbumViewModel.Events.FETCH_SELECTED_ALBUM, null))
        }
    }
}