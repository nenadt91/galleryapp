package com.typeqast.galleryapp

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import com.typeqast.core.utils.Cashing


class GalleryApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        app = this
        Cashing.initialize(this, getString(R.string.app_name))
    }

    fun checkInternetConnection(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            return (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) ||
                    (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) ||
                    (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET))
        }
        return false
    }

    companion object {
        lateinit var app: GalleryApplication
    }
}