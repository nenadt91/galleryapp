package com.typeqast.galleryapp.injection

import com.typeqast.data.dto.GalleryDTO
import com.typeqast.data.dto.PhotoDTO
import com.typeqast.data.remote.NetworkModule
import com.typeqast.data.remote.models.GalleryEntityAPI
import com.typeqast.data.remote.models.PhotoAPI
import com.typeqast.data.repositories.GalleryRepositoryImpl
import com.typeqast.data.usecases.FetchAlbumUseCaseImpl
import com.typeqast.data.usecases.FetchGalleryUseCaseImpl
import com.typeqast.data.usecases.FetchPhotoUseCaseImpl
import com.typeqast.domain.mappers.DomainMapping
import com.typeqast.domain.models.GalleryEntityDOM
import com.typeqast.domain.models.PhotoDOM
import com.typeqast.domain.usecases.FetchAlbumUseCase
import com.typeqast.domain.usecases.FetchGalleryUseCase
import com.typeqast.domain.usecases.FetchPhotoUseCase

object Injector {
    private val networkModule: NetworkModule = NetworkModule
    private val galleryDTO: DomainMapping<List<GalleryEntityDOM>, List<GalleryEntityAPI>> =
        GalleryDTO()
    private val photoDOT: DomainMapping<PhotoDOM, PhotoAPI> = PhotoDTO()
    private val galleryRepository = GalleryRepositoryImpl(networkModule, galleryDTO, photoDOT)
    private val galleryUseCase: FetchGalleryUseCase = FetchGalleryUseCaseImpl(galleryRepository)
    private val fetchAlbumUseCase: FetchAlbumUseCase = FetchAlbumUseCaseImpl(galleryRepository)
    private val fetchPhotoUseCase: FetchPhotoUseCase = FetchPhotoUseCaseImpl(galleryRepository)

    fun provideGalleryUseCase(): FetchGalleryUseCase = galleryUseCase
    fun provideAlbumUseCase(): FetchAlbumUseCase = fetchAlbumUseCase
    fun providePhotoUseCase(): FetchPhotoUseCase = fetchPhotoUseCase
}