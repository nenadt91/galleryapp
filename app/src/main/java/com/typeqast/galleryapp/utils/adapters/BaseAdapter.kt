package com.typeqast.galleryapp.utils.adapters

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.typeqast.core.constants.Status


abstract class BaseAdapter<T, VH : RecyclerView.ViewHolder> : RecyclerView.Adapter<VH>() {
    private var listener: OnPaginationListener? = null
    private var endlessScrollListener: EndlessScrollListener? = null
    private var list = ArrayList<T>()

    fun setItems(items: List<T>?) {
        if (!items.isNullOrEmpty()) {
            setEnableEndlessScrollListener(true)
            list.clear()
            notifyDataSetChanged()
            list.addAll(items)
            notifyDataSetChanged()
        }
    }

    fun addItems(items: List<T>?) {
        if (!items.isNullOrEmpty()) {
            setEnableEndlessScrollListener(true)
            list.addAll(items)
            notifyItemRangeInserted(list.size - items.size, list.size)
        }
    }

    fun addItem(item: T?) {
        if (item != null) {
            list.add(item)
            notifyItemInserted(0)
        }
    }

    fun removeAt(position: Int) {
        if (list.size > 0 && position > -1 && position < list.size) {
            list.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun clearAll() {
        list.clear()
        notifyDataSetChanged()
    }


    override fun getItemCount(): Int {
        return list.size
    }

    fun getItem(position: Int): T? {
        if (list.size > 0 && position > -1 && position < list.size) {
            return list[position]
        }
        return null
    }

    fun getItems(): ArrayList<T> = list

    fun setEndlessScrollListener(layoutManager: LinearLayoutManager): EndlessScrollListener {
        endlessScrollListener = object : EndlessScrollListener(layoutManager) {
            override fun onLoadMore(currentPage: Int) {
                setEnableEndlessScrollListener(false)
                listener?.onNextPage(currentPage)

            }
        }
        return endlessScrollListener as EndlessScrollListener

    }

    fun setEnableEndlessScrollListener(isEnabled: Boolean) {
        endlessScrollListener?.setEnabled(isEnabled)
    }

    fun handleResponseStatus(status: Int) {
        when (status) {
            Status.ERROR -> setEnableEndlessScrollListener(false)
        }
    }


    fun setOnPaginationListener(listener: OnPaginationListener) {
        this.listener = listener
    }

    interface OnPaginationListener {
        fun onNextPage(currentPage: Int)
    }

}