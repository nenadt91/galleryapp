package com.typeqast.galleryapp.utils.adapters

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.typeqast.galleryapp.BR

class BaseViewHolder<T>(private val binding: ViewDataBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(obj: T?) {
        binding.apply {
            setVariable(BR.vm, obj)
            executePendingBindings()
        }
    }

    fun getBinding(): ViewDataBinding {
        return binding
    }
}