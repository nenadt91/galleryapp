package com.typeqast.galleryapp.utils.ui

import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import com.typeqast.core.ui.fragments.CoreFragment
import com.typeqast.galleryapp.BR

abstract class BaseFragment<VDB : ViewDataBinding, VM : ViewModel> : CoreFragment<VDB, VM>() {

    override fun showMessage(message: String?) {
        (activity as BaseActivity<*, *>).showMessage(message)
    }

    override fun processUIStatus(status: Int) {
        (activity as BaseActivity<*, *>).processUIStatus(status)
    }

    override fun showDialog() {
        (activity as BaseActivity<*, *>).showDialog()
    }

    override fun dismissDialog() {
        (activity as BaseActivity<*, *>).dismissDialog()
    }

    override fun processError(error: Throwable?) {
        (activity as BaseActivity<*, *>).processError(error)
    }

    override fun provideBindingId(): Int {
        return BR.vm
    }
}