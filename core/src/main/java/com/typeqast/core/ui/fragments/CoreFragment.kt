package com.typeqast.core.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.typeqast.core.ui.ICoreView

abstract class CoreFragment<VDB : ViewDataBinding, VM : ViewModel> : Fragment(), ICoreView {
    private lateinit var binding: VDB
    private lateinit var viewModel: VM
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        initialize(inflater, container)
        return binding.root
    }

    private fun initialize(inflater: LayoutInflater, container: ViewGroup?) {
        setupViewModel()
        setupDataBinding(inflater, container)
    }

    private fun setupDataBinding(inflater: LayoutInflater, container: ViewGroup?) {
        binding = DataBindingUtil.inflate(inflater, provideLayout(), container, false)
        binding.apply {
            lifecycleOwner = viewLifecycleOwner
            setVariable(provideBindingId(), viewModel)
        }
    }

    private fun setupViewModel() {
        viewModel = provideViewModel()
    }

    fun getViewModel() = viewModel

    fun getBinding() = binding

    abstract fun provideViewModel(): VM

    abstract fun provideLayout(): Int

}