package com.typeqast.core.ui


interface ICoreView {
    fun provideBindingId(): Int = 0
    fun showMessage(message: String?)
    fun processUIStatus(status: Int)
    fun showDialog()
    fun dismissDialog()
    fun processError(error: Throwable?)
    fun provideTAG(): String
}