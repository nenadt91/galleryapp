package com.typeqast.core.ui.activites

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import com.typeqast.core.ui.ICoreView

abstract class CoreActivity<VDB : ViewDataBinding, VM : ViewModel> : AppCompatActivity(),
    ICoreView {
    private lateinit var binding: VDB
    private lateinit var viewModel: VM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initialize()
    }

    private fun initialize() {
        setupViewModel()
        setupDataBinding()
    }

    private fun setupDataBinding() {
        binding = DataBindingUtil.setContentView(this, provideLayout())
        binding.apply {
            lifecycleOwner = this@CoreActivity
            setVariable(provideBindingId(), viewModel)
        }
    }

    private fun setupViewModel() {
        viewModel = provideViewModel()
    }

    fun getViewModel() = viewModel

    fun getBinding() = binding

    abstract fun provideViewModel(): VM

    abstract fun provideLayout(): Int
}