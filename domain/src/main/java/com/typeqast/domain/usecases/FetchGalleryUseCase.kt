package com.typeqast.domain.usecases

import androidx.lifecycle.LiveData
import com.typeqast.domain.models.GalleryEntityDOM
import com.typeqast.domain.wrappers.Resource

interface FetchGalleryUseCase {
    suspend fun execute(hasInternetConnection: Boolean): LiveData<Resource<List<GalleryEntityDOM>>>
}