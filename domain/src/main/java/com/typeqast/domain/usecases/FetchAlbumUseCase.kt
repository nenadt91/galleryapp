package com.typeqast.domain.usecases

import androidx.lifecycle.LiveData
import com.typeqast.domain.models.GalleryEntityDOM
import com.typeqast.domain.wrappers.Resource

interface FetchAlbumUseCase {
    suspend fun execute(
        hasInternetConnection: Boolean,
        albumId: Int
    ): LiveData<Resource<GalleryEntityDOM?>>
}