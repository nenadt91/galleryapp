package com.typeqast.domain.usecases

import androidx.lifecycle.LiveData
import com.typeqast.domain.models.PhotoDOM
import com.typeqast.domain.wrappers.Resource

interface FetchPhotoUseCase {
    suspend fun execute(
        hasInternetConnection: Boolean,
        imageUrl: String
    ): LiveData<Resource<PhotoDOM>>
}