package com.typeqast.domain.models

data class GalleryEntityDOM(
    val id: Int,
    val name: String,
    val coverImage: String,
    val album: List<AlbumDOM>
)
