package com.typeqast.domain.repositores

import com.typeqast.domain.models.GalleryEntityDOM
import com.typeqast.domain.models.PhotoDOM


interface GalleryRepository {
    suspend fun fetchAllImages(): List<GalleryEntityDOM>
    suspend fun fetchSelectedAlbum(id: Int): GalleryEntityDOM?
    suspend fun fetchImage(url: String): PhotoDOM
}