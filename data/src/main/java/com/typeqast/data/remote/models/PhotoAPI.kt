package com.typeqast.data.remote.models

import com.google.gson.annotations.SerializedName

data class PhotoAPI(
    @SerializedName("name") val name: String,
    @SerializedName("thumbnailImage") val thumbnailUrl: String,
    @SerializedName("fullImage") val photoUrl: String
)