package com.typeqast.data.remote.models

import com.google.gson.annotations.SerializedName

data class Album(
    @SerializedName("name") val name: String,
    @SerializedName("thumbnailImage") val thumbnailImage: String,
    @SerializedName("imageUrlPath") val imageUrlPath: String
)
