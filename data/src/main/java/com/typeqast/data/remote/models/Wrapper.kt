package com.typeqast.data.remote.models

import com.google.gson.annotations.SerializedName

data class Wrapper(@SerializedName("gallery") val gallery: List<GalleryEntityAPI>)
