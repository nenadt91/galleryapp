package com.typeqast.data.remote.models

import com.google.gson.annotations.SerializedName

data class GalleryEntityAPI(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("coverImage") val coverImage: String,
    @SerializedName("album") val album: List<Album>
)