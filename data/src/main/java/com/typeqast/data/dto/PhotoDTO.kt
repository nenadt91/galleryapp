package com.typeqast.data.dto

import com.typeqast.data.remote.models.PhotoAPI
import com.typeqast.domain.mappers.DomainMapping
import com.typeqast.domain.models.PhotoDOM

class PhotoDTO : DomainMapping<PhotoDOM, PhotoAPI> {
    override fun mapToDomain(remote: PhotoAPI): PhotoDOM {
        return PhotoDOM(remote.name, remote.thumbnailUrl, remote.photoUrl)
    }
}