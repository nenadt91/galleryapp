package com.typeqast.data.repositories

import com.typeqast.data.remote.NetworkModule
import com.typeqast.data.remote.models.GalleryEntityAPI
import com.typeqast.data.remote.models.PhotoAPI
import com.typeqast.domain.mappers.DomainMapping
import com.typeqast.domain.models.GalleryEntityDOM
import com.typeqast.domain.models.PhotoDOM
import com.typeqast.domain.repositores.GalleryRepository

class GalleryRepositoryImpl(
    private val networkModule: NetworkModule,
    private val galleryMapper: DomainMapping<List<GalleryEntityDOM>, List<GalleryEntityAPI>>,
    private val photoMapper: DomainMapping<PhotoDOM, PhotoAPI>
) : GalleryRepository {
    override suspend fun fetchAllImages(): List<GalleryEntityDOM> {
        val allGalleryImages = networkModule.remote.getAllAlbums()
        return galleryMapper.mapToDomain(allGalleryImages.gallery)
    }

    override suspend fun fetchSelectedAlbum(id: Int): GalleryEntityDOM? {
        val fetchAllImages = fetchAllImages()
        return fetchAllImages.find { it.id == id }
    }

    override suspend fun fetchImage(url: String): PhotoDOM {
        val photo = networkModule.remote.getImage(url)
        return photoMapper.mapToDomain(photo)

    }
}