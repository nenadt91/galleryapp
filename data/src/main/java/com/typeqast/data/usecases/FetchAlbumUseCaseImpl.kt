package com.typeqast.data.usecases

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.typeqast.domain.models.GalleryEntityDOM
import com.typeqast.domain.repositores.GalleryRepository
import com.typeqast.domain.usecases.FetchAlbumUseCase
import com.typeqast.domain.wrappers.Resource

class FetchAlbumUseCaseImpl(
    private val galleryRepository: GalleryRepository,
) : FetchAlbumUseCase {
    override suspend fun execute(
        hasInternetConnection: Boolean,
        albumId: Int
    ): LiveData<Resource<GalleryEntityDOM?>> {
        return liveData {
            try {
                emit(Resource.loading<GalleryEntityDOM?>())
                if (!hasInternetConnection) {
                    emit(Resource.noInternet(null, null))
                    return@liveData
                }
                emit(Resource.success(galleryRepository.fetchSelectedAlbum(albumId), null))
            } catch (error: Error) {
                emit(Resource.error(null, error))
            }
        }
    }
}