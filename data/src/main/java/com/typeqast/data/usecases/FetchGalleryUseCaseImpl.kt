package com.typeqast.data.usecases

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.typeqast.domain.models.GalleryEntityDOM
import com.typeqast.domain.repositores.GalleryRepository
import com.typeqast.domain.usecases.FetchGalleryUseCase
import com.typeqast.domain.wrappers.Resource

class FetchGalleryUseCaseImpl(
    private val galleryRepository: GalleryRepository,
) : FetchGalleryUseCase {
    override suspend fun execute(hasInternetConnection: Boolean): LiveData<Resource<List<GalleryEntityDOM>>> {
        return liveData {
            try {
                emit(Resource.loading<List<GalleryEntityDOM>>())
                if (!hasInternetConnection) {
                    emit(Resource.noInternet(null, null))
                    return@liveData
                }
                emit(Resource.success(galleryRepository.fetchAllImages(), null))
            } catch (error: Error) {
                emit(Resource.error(null, error))
            }
        }
    }
}